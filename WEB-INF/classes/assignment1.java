import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Scanner;
import javax.servlet.*;
import javax.servlet.http.*;
import java.time.LocalDateTime;
import java.text.SimpleDateFormat;   
import java.io.*;
import java.util.Random;
import java.util.Date;
import java.util.Calendar;
 
@WebServlet(name = "assignment1", urlPatterns = "/assignment1")
public class assignment1 extends HttpServlet

	{

		private Seat[][] seat = new Seat[8][8];
		private Seat[][] usedSeats = new Seat[8][8];
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			response.setContentType("text/html;charset=UTF-8");
			int test = 1;
			if(request.getParameter("username") != null)
			{
			
				if(numOfBooking(request.getParameter("username")) == true)
				{
						Seat seats = new Seat();
						String s = request.getParameter("seat");
						String userName = request.getParameter("username"); 
						String phoneNumber = request.getParameter("phone");
						String fName = request.getParameter("Fname");
						String lName = request.getParameter("Lname");
						String address = request.getParameter("address");
						String email = request.getParameter("email");
						String code = request.getParameter("code");
						
						seats.setId(userName);
						seats.setFirstName(fName);
						seats.setLastName(lName);
						seats.setPhone(phoneNumber);
						seats.setAddress(address);
						seats.setEmail(email);
						seats.setTime(DateAndTime());
						seats.setPosition(s);

						String temp = request.getParameter("seat");
						int x = search(temp);  
						String tempFirst ="";						
						tempFirst += temp.charAt(1);							
						int y = Integer.valueOf(tempFirst);	
						x += 1;
						y -= 1;
						usedSeats[x][y] = seats;						 

						for(int i = 0; i < 8; i++)
						{
							for(int j = 0; j < 8; j++)
							{
								if(usedSeats[i][j] != null && usedSeats[i][j].getId().equals(userName))
								{
									if(request.getParameter("phone") != null)
									{ 
										usedSeats[i][j].setPhone(request.getParameter("phone"));
									}
									if(request.getParameter("address") != null)
									{ 
										usedSeats[i][j].setAddress(request.getParameter("address"));
									}
									if(request.getParameter("email") != null)
									{ 
										usedSeats[i][j].setEmail(request.getParameter("email"));
									}
									if(request.getParameter("Fname") != null)
									{ 
										usedSeats[i][j].setFirstName(request.getParameter("Fname"));
									}
									if(request.getParameter("Lname") != null)
									{ 
										usedSeats[i][j].setLastName(request.getParameter("Lname"));
									}
									
								}
							}

							saveData();		
						}
				}
			}
			response.getWriter().println(header());
			response.getWriter().println(getManinPage());
			response.getWriter().println(makeFooter());
		}
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
		{
			String seatPostion = request.getParameter("seat");		
			if(seatPostion == null)
			{
				response.getWriter().println(header());
				response.getWriter().println(getManinPage());
			}
			else
			{
				response.getWriter().println(getBookingPage(seatPostion));  
				
			}
			response.getWriter().println(makeFooter());

		}
		public String header(){ 
			String headerInterface = "<!DOCTYPE html><html class = Ahmed><head><meta charset=\"UTF-8\">";
			headerInterface += "<link rel=\"stylesheet\" href=\"stylee.css\" type=\"text/css\">";
			headerInterface += "</head><body>";
			headerInterface +="<div class=\"top\"><h1 class=\"btn\">Booking site pty LTD</h1></div> ";
			headerInterface +="<div><h1 id=\"studentNumber\">c3277545</h1></div> ";
			headerInterface +="<div><h4>Please seclect one of non red seats </h4></div> ";
			headerInterface +="<div class=\"content\">";
			headerInterface +="<br></br>";
			return headerInterface;
		}
		
			public String getTable(){
				String[] letters = {"A","B","C","D","E","F","G","H"};
			String table = "<table id=\"seats\"> <tr>";
			for(int i = 0; i < 8; i++)                                       
			{
				if(i==0){table += "<td class=\"titles\"></td>";}
				table += "<td class=\"titles\">" +(i+1)+"</td>";
			}

			table += "</tr><tr>";
			for(int i = 0; i < 8; i++){
				table += "<td class=\"titles\">" + letters[i]+"</td>";
				for(int j = 1; j <= 8; j++){

					String box = "";
					box += letters[i] + (j);                                
					if(usedSeats[i][j-1] != null)
						{
							table += "<td class=\"booked box\" id=\"" + box + "\">" + box +"</td>";
						}
					else                                                    
						{
							table += "<td class=\"free box\" id=\"" + box + "\">" + box +"</td>";
						}
				}
				table += "</tr><tr>";
			}
			table += "</table>";
			return table;

		}
		
		private String securityCode() {
			
			String format = "";
			String securityCode = Long.toHexString(Double.doubleToLongBits(Math.random())).substring(5, 10).toUpperCase();
			format += "<h3 class =Ahmed >"+securityCode+"</h3>";
			return format;
		}
		private String DateAndTime(){  
			Date date = Calendar.getInstance().getTime();  
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
			String strDate = dateFormat.format(date);  
			return strDate;
		}
		
		private String makeFooter()
		{
			String footer = "";
			footer += "<footer>";
			footer += "<hr>";
			footer += "<p class=\"A4\">Ahmed Alkhazraji c3277545@uon.edu.au</p>";
			footer += "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>";
			footer += "<script src=\"scripts_seats.js\"></script>";
			footer += "</ footer>";
			footer += "<hr>";
			footer += "</html>";
			return footer;
		}
		
			private String getBookingPage(String seatPostion)
			{
				String bookingForm ="";
				int x = search(seatPostion);  
				String tempFirst ="";						
				tempFirst += seatPostion.charAt(1);							
				int y = Integer.valueOf(tempFirst);
				x += 1;
				y -= 1;
				System.out.println("the index of rows is " + x);
				System.out.println("the index of the coloms " + y);
				if(usedSeats[x][y] != null)
				{
					bookingForm += "<h1>Seat " + seatPostion + " is booked </h1>";
					bookingForm += "<p> user first name: " + usedSeats[x][y].firstName() + " </P>";
					bookingForm += "<p> user last name: " + usedSeats[x][y].lastName() + " </P>";
					bookingForm += "<p> user email: " + usedSeats[x][y].getEmail() + " </P>";
					bookingForm += "<p> user phone number: " + usedSeats[x][y].getPhone() + " </P>";
					bookingForm += "<p> user address: " + usedSeats[x][y].getAddress() + " </P>";
					bookingForm += "<p> user ID: " + usedSeats[x][y].getId() + " </P>";
					bookingForm += "<p> Booking time: " + usedSeats[x][y].getTime() + "</p>";
				}
				else
				{
					bookingForm = "<div class=\"page-title\"><h1>On the prosses of booking seat number " + seatPostion + " </h1></div>";
					bookingForm += "<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>";
					bookingForm += "<script src=\"scripts_booking_page.js\"></script>";
					bookingForm += "<h5>";
					bookingForm += "<link rel=\"stylesheet\" href=\"stylee.css\" type=\"text/css\">";
					bookingForm += "<form id=\"booking\" action=\"assignment1\"\" method=\"POST\">";
					bookingForm += "<p><label for=\"username\">Please enter your first name </label></p><input class=\"input\" type=\"text\" id=\"Fname\" name=\"Fname\" />";
					bookingForm += "<p><label for=\"username\">Please enter your last name </label></p><input class=\"input\" type=\"text\" id=\"Lname\" name=\"Lname\" />";
					bookingForm += "<p><label for=\"seat\">the selected seat is in the box below</label><br><input class=\"input\" type=\"text\" id=\"seat\" name=\"seat\" value=\"" + seatPostion + "\" </p>";
					bookingForm += "<p><label for=\"address\">Please enter your address including the house number</label></p><input class=\"input\" type=\"text\" id=\"address\" name=\"address\" />";
					bookingForm += "<p><label for=\"phone\">Please enter your phone </label></p><input class=\"input\" type=\"text\" id=\"phone\" name=\"phone\" />";
					bookingForm += "<p><label for=\"username\">Please enter your user ID </label></p><input class=\"input\" type=\"text\" id=\"username\" name=\"username\" />";
					bookingForm += "<p><label for=\"email\">Please enter your email</label></p><input class=\"input\" type=\"text\" id=\"email\" name=\"email\" />";
					bookingForm += "<p><label for=\"code\">Please enter the follwing scurity code "+ securityCode() +" in the box below</p></label></p><input class=\"input\" type=\"text\" id=\"code\" name=\"code\" /> <br>";
					bookingForm += "<h4>" +DateAndTime()+" </h4>";
					bookingForm += "<input type=\"submit\" id=\"submit-btn\" value=\"Submit\" /> <input type=\"reset\" id=\"reset-btn\" value=\"Clear\" />";
					bookingForm += "</h5>";
					bookingForm += "</form>";
				}
				return bookingForm;
			}
		
		private String getManinPage()
		{
			String a ="";
			a += getTable();
			return a;
		}
		
		private boolean numOfBooking(String id)
		{
			int count = 0;
			for(int i = 0; i < 8; i++)
			{
				
				for(int j = 0; j < 8; j++)
				{
					if(usedSeats[i][j] != null)
					{
						if(id.equals(usedSeats[i][j].getId()))
						{ 
							count++; 
						}
					}
				}

			}
			if(count < 3)
			{ 
				return true;
			}
			else
			{
				return false;
			}

		}
		
		private int search(String x)
		{
			String[] rows = {"A","B","C","D","E","F","G","H"};
			String temp = "";
			temp += x.charAt(0);  
			int tempNum = 0;
				for(int i = 0; i < rows.length; i++)
				{
					if(rows[i].equals(temp))
					{
						tempNum = i-1; 
					}
				}
			return tempNum;
		}
		
		public void saveData()
		{ 
			PrintWriter outputStream = null;
			String data = "Seats-Data.txt";
			try
			{
				outputStream = new PrintWriter(data);
				for(int i =0; i < 8; i++)
				{
					for(int j =0; j < 8; j++)
					{
						if(usedSeats[i][j] != null)
						{
							String output = "Time and Date";
							outputStream.println("Seat");
							outputStream.println(usedSeats[i][j].getPosition());
							 output = "-1";
							if(usedSeats[i][j].getId() != null)
								{
									outputStream.println(usedSeats[i][j].getId());
								}
							if(usedSeats[i][j].firstName() != null)
								{
									outputStream.println(usedSeats[i][j].firstName());
								}
							if(usedSeats[i][j].lastName() != null)
								{
									outputStream.println(usedSeats[i][j].lastName());
								}
							if(usedSeats[i][j].getAddress() != null)
								{
									outputStream.println(usedSeats[i][j].getAddress());
								}
							if(usedSeats[i][j].getEmail() != null)
								{
									outputStream.println(usedSeats[i][j].getEmail());
								}
							if(usedSeats[i][j].getPhone() != null)
								{
									outputStream.println(usedSeats[i][j].getPhone());
								}
							if(usedSeats[i][j].getPosition() != null)
								{
									outputStream.println(usedSeats[i][j].getPosition());
								}
							if(usedSeats[i][j].getTime() != null)
								{
									outputStream.println(usedSeats[i][j].getTime());
								}
						}
					}
				}
				outputStream.close();
			}
			catch (FileNotFoundException e){}
		}
		
		
	}