//    assignment1 
//    Ahmed Al-khazraji c3277545
//    2/04/2021

public class Seat{

	private String fName;
	private String lName;
	private String address;
    private String position;
    private String userID;
    private String email;
    private String phone;
	private String time;

    Seat(){
        userID = null;
        email = null;
        address = null;
		fName = null;
		lName = null;
		time =null;
		phone =null;
		position = null;
    }

    public void setPosition(String pos){
        position = pos;
    }
    public void setId(String id){
        userID = id;
    }
	
	public void setFirstName(String f){
        fName = f;
    }
	
	public void setLastName(String l){
        lName = l;
    }
    public void setEmail(String mail){
        email = mail;
    }
    public void setAddress(String A){
        address = A;
    }
    public void setPhone(String number){
        phone = number;
    }
	public void setTime(String x){
        time = x;
    }

	
	
    public String getId(){
        return userID;
    }
	
	public String firstName(){
        return fName;
    }
	
	public String lastName(){
        return lName;
    }
    public String getPhone(){
        return phone;
    }
    public String getPosition(){
        return position;
    }
    public String getEmail(){
        return email;
    }
    public String getAddress(){
        return address;
    }
	public String getTime(){
        return time;
    }
    

}